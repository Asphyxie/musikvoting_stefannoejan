package Datenbank;

import Logik.Song;
import Logik.User;
import Logik.Vote;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import Design.Oberfl�che;

public class SQL {

	/*
	 * Song wird der SQL Datenbank hinzugef�gt.
	 */
	public static void addSong(Song lied) {

		try {
			// Attribute
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/test?";
			String user = "testuser";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// PreparedStatement wird ohne Werte initialisiert.
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO t_song ('Genre', 'Bandname', 'Titel') VALUES (?,?,?)");
			// PreparedStatement erh�lt Werte
			ps.setString(1, lied.getGenre());
			ps.setString(2, lied.getBand());
			ps.setString(3, lied.getTitel());
			// SQL-Anweisung (PreparedStatement) wird ausgef�hrt und f�gt Song der Datenbank
			// hinzu
			ps.executeUpdate();

			// Verbindung schlie�en
			con.close();
		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

	} // Ende von addSong

	/*
	 * Gast wird der SQL Datenbank hinzugef�gt.
	 */
	public static void addGast(User gast) {

		try {
			// Attribute
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/test?";
			String user = "testuser";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// PreparedStatement wird ohne Werte initialisiert.
			PreparedStatement ps = con.prepareStatement("INSERT INTO t_gast(�gastname�) VALUES('?')");
			// PreparedStatement erh�lt Werte
			ps.setString(1, gast.getName());
			// SQL-Anweisung (PreparedStatement) wird ausgef�hrt und f�gt Gast der Datenbank
			// hinzu
			ps.executeUpdate();

			// Verbindung schlie�en
			con.close();
		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

	} // Ende von addGast

	/*
	 * Vote wird der SQL Datenbank hinzugef�gt.
	 */
	public static void addVote(Vote vote) {
		
		Boolean allowVote = false;
		
		try {
			// Attribute
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/test?";
			String user = "testuser";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// �berpr�fung auf den und Haus
			if((getTitel(vote.getId_User()) == "Ser") || (getHausname(vote.getId_User()) != "")) {
				allowVote = true;
			}
			
			// Hier wird auf die
			if(allowVote == true) {
				// PreparedStatement wird ohne Werte initialisiert.
				PreparedStatement ps = con
						.prepareStatement("INSERT INTO t_song_gast(pf_gast_id,pf_song_id) VALUES('?','?')");
				// PreparedStatement erh�lt Werte
				ps.setInt(1, vote.getId_User());
				ps.setInt(2, vote.getId_Song());
				// SQL-Anweisung (PreparedStatement) wird ausgef�hrt und f�gt Vote der Datenbank
				// hinzu
				ps.executeUpdate();
			}

			// Verbindung schlie�en
			con.close();
		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

	} // Ende von addVote

	/*
	 * Hier wird ein resultset der Musiktitel ausgegeben
	 */
	public static Song[] getSongList() {

		// Resultset wird null-intialisiert, wird sp�ter ja ge�ndert, aber lieber null
		// als nichts.
		ResultSet rs = null;

		// Song-Array initialisiert. 1000 Stellen, da es nicht m�glich ist, die L�nge
		// eines ResultSets herauszufinden.
		// Nach dem Motto "Wird schon reichen"
		Song[] songs = new Song[1000];

		// Z�hler f�r Casting initialisiert
		int i = 0;

		try {
			// Attribute f�r MySQL werden Initialisiert mit Benutzer "Root".
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String user = "root";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// Resultset wird Scrollbar und Updatebar gemacht (Denglisch FTW!)
			// Um ehrlich zu sein habe ich von dem Statement hier keine Ahnung, habs im
			// Internet gelesen, sah wichtig aus, Strg+C, Strg+V, kann ja nicht schaden.
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

			// Statement um Liederliste zu erhalten Liederliste wird ausgef�hrt und im
			// ResultSet 'rs' gespeichert.
			rs = stmt.executeQuery("SELECT Bandname, Titel, Genre FROM t_song");

			// Resultset wird zu einem Song-Array gecasted um die Bildschirmausgabe zu
			// vereinfachen.
			while (rs.next()) {
				songs[i++] = new Song(rs.getString(1), rs.getString(2), rs.getString(3));
			}

			// Verbindung wird geschlossen, damit sie im hintergrund nicht weiterl�uft.
			// ResultSet wird gel�scht.
			con.close();

		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben (als ob wir "jemals" Fehler machen w�rden)
		}

		// Das Gecastete Song-Array wird zur�ckgegeben.
		return songs;

	} // Ende von getSongList

	/*
	 * Hier wird ein Song-Array der Musiktitel ausgegeben
	 */
	public static Song[] getRanking() {

		// Resultset wird null-intialisiert um es im Fall eines Fehlers trotzdem zu
		// returnen
		ResultSet rs = null;

		// Wieder der z�hler (siehe getSongList)
		int i = 0;

		// Pi mal daumen werden 1000 Songs reichen, so viel kann Helene Fischer ja nicht
		// gesungen haben. Glaube ich. Hoffentlich. Doch lieber 10000?
		Song[] songs = new Song[1000];

		try {
			// Attribute
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String user = "root";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// Resultset wird Scrollbar und Updatebar gemacht.
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

			// Statement um ein Ranking zu erhalten wird ausgef�hrt und im ResultSet 'rs'
			// gespeichert, besserer SQL-Befehl folgt.
			/*
			 * [DER STATEMENT BEFEHL MUSS GE�NDERT WERDEN] [DER STATEMENT BEFEHL MUSS
			 * GE�NDERT WERDEN] [DER STATEMENT BEFEHL MUSS GE�NDERT WERDEN]
			 */
			rs = stmt.executeQuery("");

			// Umcasten zu einem Song-Array f�r einfachere Ausgabe der Rolf Zuckowski
			// Hitsongs. Der Kracher auf jeder Party.
			while (rs.next()) {
				songs[i++] = new Song(rs.getString(1), rs.getString(2), rs.getString(3));
			}

			// Verbindung schlie�en und ResultSet l�schen
			con.close();

		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

		// Das Song Array wird zur�ckgegeben
		return songs;

	} // Ende von getSongList
	
	/*
	 * Hier wird ein User-Array der benutzer zur�ckgegeben um den Login zu �berpr�fen
	 */
	public static User[] getUser() {

		// Resultset wird null-intialisiert um es im Fall eines Fehlers trotzdem zu
		// returnen
		ResultSet rs = null;

		// Wieder der z�hler (siehe getSongList)
		int i = 0;

		// Erstellung eines User Arrays mit 1000 G�sten, wird ne gro�e Party, der Berg
		// z�hlt ja eh als zwei... oder drei...
		User[] users = new User[1000];

		try {
			// Attribute
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String user = "root";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// Resultset wird Scrollbar und Updatebar gemacht.
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

			// Gastnamen werden als Resultset aus der Datenbank geholt. Der Titel ist jedem eindeutigem Gastnamen zuzuordnen.
			// User Array gecastet
			rs = stmt.executeQuery("SELECT gastname FROM t_gast");
			// Umcasten zu einem User-Array f�r einfachere Ausgabe der Henkerlis....
			// G�steliste
			while (rs.next()) {
				users[i++] = new User(rs.getString(1));
			}

			// Verbindung schlie�en und ResultSet l�schen
			con.close();

		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

		// Das User Array wird zur�ckgegeben
		return users;

	} // Ende von getUser
	
	/*
	 * Hier wird der Titel eines Benutzers zur�ckgegeben um zu schauen, ob er Voten darf.
	 */
	public static String getTitel(int id) {

		// Resultset wird null-intialisiert um es im Fall eines Fehlers trotzdem zu
		// returnen
		ResultSet rs = null;

		// Wieder der z�hler (siehe getSongList)
		int i = 0;

		// Leerer String f�r den Titel des Gasts
		String titel = "";

		try {
			// Attribute
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String user = "root";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// Resultset wird Scrollbar und Updatebar gemacht.
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

			// Titel wird vom ResultSet in den String �bergeben.
			rs = stmt.executeQuery("SELECT adelstitel FROM t_gast WHERE p_id = " + id);
			while (rs.next()) {
				titel = rs.getString(1);
			}

			// Verbindung schlie�en und ResultSet l�schen
			con.close();

		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

		// Das Titel String wird zur�ckgegeben
		return titel;

	} // Ende von getUser
	
	public static String getHausname(int id) {

		// Resultset wird null-intialisiert um es im Fall eines Fehlers trotzdem zu
		// returnen
		ResultSet rs = null;

		// Wieder der z�hler (siehe getSongList)
		int i = 0;

		// Leerer String f�r den Haus des Gasts
		// Wenn er Pech hat kann er halt nicht voten, daf�r bringt er das (nat�rlich sauber programmierte) Programm
		// nicht zum Absturz
		String Haus = "";

		try {
			// Attribute
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String user = "root";
			String passwort = "";

			// JDBC-Treiber laden
			Class.forName(driver);

			// Verbindung herstellen
			Connection con;
			con = DriverManager.getConnection(url, user, passwort);

			// Resultset wird Scrollbar und Updatebar gemacht.
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			// Resultset mit einem ganzen Datensatz wird zu einem String gecastet.
			rs = stmt.executeQuery("SELECT hausname FROM t_gast WHERE p_id = " + id);
			while (rs.next()) {
				Haus = rs.getString(1);
			}

			// Verbindung schlie�en und ResultSet l�schen
			con.close();

		} catch (Exception e) { // Fehler abfangen
			e.printStackTrace(); // Fehlermeldung ausgeben
		}

		// Der Haus String wird zur�ckgegeben
		return Haus;

	} // Ende von getUser

}// Ende der Klasse SQL