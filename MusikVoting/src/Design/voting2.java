package Design;

import Datenbank.SQL;
import Logik.Song;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JTextPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.AbstractListModel;

public class voting2 extends JPanel {

	/**
	 * Create the panel.
	 */
	public voting2() {
		
		SQL a = new SQL();
		Song[] song = new Song[a.getSongList().length];
		song = a.getSongList();
		
		setLayout(new BorderLayout(0, 0));
		
		Object[] data = new Object[5];	
		
		DefaultListModel listModel = new DefaultListModel();
		
		for(int i = 0; song.length > i; i++) {
			listModel.addElement(song[i]);
			
		}
		//listModel.addElement("Helene Fischer");
		
		JList list = new JList(listModel);
		JScrollPane listScroller = new JScrollPane(list);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		
		add(listScroller, BorderLayout.CENTER);
		
		JButton btnNewButton = new JButton("Voten");
		add(btnNewButton, BorderLayout.SOUTH);
		

	}

}
