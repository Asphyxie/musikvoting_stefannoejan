package Design;

import Datenbank.SQL;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

public class GUI extends JFrame {

	private JPanel contentPane;
	final static String WUNSCH = "Wunsch";
    final static String VOTING = "Voting";
    final static String RANKING = "Ranking";
    final static int extraWindowWidth = 100;
   
    
    public void addComponentToPane(Container pane) {
        JTabbedPane tabbedPane = new JTabbedPane();
        
        JPanel card1 = new wunsch2() {
        	public Dimension getPreferredSize() {
        		Dimension size = super.getPreferredSize();
        		size.width += extraWindowWidth;
        		return size;
        	}
        };
        
      
     
        JPanel card2 = new voting2();
        
        JPanel card3 = new ranking();
        
        tabbedPane.addTab(WUNSCH, card1);
        tabbedPane.addTab(VOTING, card2);
        tabbedPane.add(RANKING, card3);
        pane.add(tabbedPane, BorderLayout.CENTER);
    }    
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.addComponentToPane(frame.getContentPane());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
