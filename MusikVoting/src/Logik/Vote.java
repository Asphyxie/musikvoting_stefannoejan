package Logik;

public class Vote {
	int id_Song;
	int id_User;
	
	public Vote(int id_Song, int id_User) {
		this.id_Song = id_Song;
		this.id_User = id_User;
	}

	public int getId_Song() {
		return id_Song;
	}

	public void setId_Song(int id_Song) {
		this.id_Song = id_Song;
	}

	public int getId_User() {
		return id_User;
	}

	public void setId_User(int id_User) {
		this.id_User = id_User;
	}
	
}
