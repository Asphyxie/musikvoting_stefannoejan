package Logik;

public class User {
	int id;
	String name;
	String adelstitel;
	String Hausname;
	
	public String getHausname() {
		return Hausname;
	}

	public void setHausname(String hausname) {
		Hausname = hausname;
	}

	public String getAdelstitel() {
		return adelstitel;
	}

	public void setAdelstitel(String adelstitel) {
		this.adelstitel = adelstitel;
	}

	public User(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
